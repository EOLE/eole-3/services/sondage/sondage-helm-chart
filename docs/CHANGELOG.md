# Changelog

### [1.7.2](https://gitlab.mim-libre.fr/EOLE/eole-3/services/sondage/sondage-helm-chart/compare/release/1.7.1...release/1.7.2) (2025-01-30)


### Bug Fixes

* update appVersion to 2.1.1 ([05d08a4](https://gitlab.mim-libre.fr/EOLE/eole-3/services/sondage/sondage-helm-chart/commit/05d08a42fe40ee1db3e9ca270ae47e3d2116e4d9))

### [1.7.1](https://gitlab.mim-libre.fr/EOLE/eole-3/services/sondage/sondage-helm-chart/compare/release/1.7.0...release/1.7.1) (2024-10-08)


### Bug Fixes

* deploy appVersion to 2.2.0 ([9961ea1](https://gitlab.mim-libre.fr/EOLE/eole-3/services/sondage/sondage-helm-chart/commit/9961ea16db27eaabd280f9f50fd7ae14108e9247))

## [1.7.0](https://gitlab.mim-libre.fr/EOLE/eole-3/services/sondage/sondage-helm-chart/compare/release/1.6.0...release/1.7.0) (2024-06-12)


### Features

* update helm chart appVersion to 1.11.0 ([da2a1b7](https://gitlab.mim-libre.fr/EOLE/eole-3/services/sondage/sondage-helm-chart/commit/da2a1b7ac68334b05738659a7b3407c64102a4d3))


### Bug Fixes

* restore testing tag ([d7a63f5](https://gitlab.mim-libre.fr/EOLE/eole-3/services/sondage/sondage-helm-chart/commit/d7a63f5c45df802feb7651ca979b9eaa32fe374f))
* wait 5 minutes before killing pod ([98c178f](https://gitlab.mim-libre.fr/EOLE/eole-3/services/sondage/sondage-helm-chart/commit/98c178f67f894af3ee2ed32335b6fb3d991684c8))

## [1.6.0](https://gitlab.mim-libre.fr/EOLE/eole-3/services/sondage/sondage-helm-chart/compare/release/1.5.0...release/1.6.0) (2024-01-16)


### Features

* update app version to 1.10.0 ([f1705f0](https://gitlab.mim-libre.fr/EOLE/eole-3/services/sondage/sondage-helm-chart/commit/f1705f0bc29dfa84ba97a339d8148f0870be6bde))

## [1.5.0](https://gitlab.mim-libre.fr/EOLE/eole-3/services/sondage/sondage-helm-chart/compare/release/1.4.0...release/1.5.0) (2023-10-17)


### Features

* new stable helm for app version 1.9.0 ([a1eae5d](https://gitlab.mim-libre.fr/EOLE/eole-3/services/sondage/sondage-helm-chart/commit/a1eae5dda6307f5ea3d2b3bf0611194644efe612))


### Bug Fixes

* appversion reference testing image ([bed44d4](https://gitlab.mim-libre.fr/EOLE/eole-3/services/sondage/sondage-helm-chart/commit/bed44d4cc93755830be7d6b78b61d9e026006634))

## [1.4.0](https://gitlab.mim-libre.fr/EOLE/eole-3/services/sondage/sondage-helm-chart/compare/release/1.3.0...release/1.4.0) (2023-08-29)


### Features

* new chart version for appversion 1.8.1 ([2d6f488](https://gitlab.mim-libre.fr/EOLE/eole-3/services/sondage/sondage-helm-chart/commit/2d6f48822bf8854d5f90ca7367eadfa97de01982))

## [1.3.0](https://gitlab.mim-libre.fr/EOLE/eole-3/services/sondage/sondage-helm-chart/compare/release/1.2.1...release/1.3.0) (2023-08-25)


### Features

* new stable version for appversion 1.8.0 ([7a133f2](https://gitlab.mim-libre.fr/EOLE/eole-3/services/sondage/sondage-helm-chart/commit/7a133f2a717dcc80d8fc9c4a8732205441974b10))

### [1.2.1](https://gitlab.mim-libre.fr/EOLE/eole-3/services/sondage/sondage-helm-chart/compare/release/1.2.0...release/1.2.1) (2023-05-09)


### Bug Fixes

* add more description in readme ([6ee9415](https://gitlab.mim-libre.fr/EOLE/eole-3/services/sondage/sondage-helm-chart/commit/6ee941502a829ad104673e5cc2c2f9588348e085))
* dev chart version deploy dev app version ([dc4a33c](https://gitlab.mim-libre.fr/EOLE/eole-3/services/sondage/sondage-helm-chart/commit/dc4a33c5d6e56a911a86606377e5802f037b6edc))
* helm testing deploy testing app version ([ba95674](https://gitlab.mim-libre.fr/EOLE/eole-3/services/sondage/sondage-helm-chart/commit/ba95674531d79868addafda5990e51914a9b0a66))
* publish new helm stable version ([bfcaf6d](https://gitlab.mim-libre.fr/EOLE/eole-3/services/sondage/sondage-helm-chart/commit/bfcaf6dedf58cc621a509b16a60ee89c8e58414c))

## [1.2.0](https://gitlab.mim-libre.fr/EOLE/eole-3/services/sondage/sondage-helm-chart/compare/release/1.1.0...release/1.2.0) (2023-04-18)


### Features

* publish stable version for sondage 1.7.0 ([702c749](https://gitlab.mim-libre.fr/EOLE/eole-3/services/sondage/sondage-helm-chart/commit/702c749cf8ce3b6160c65945b1773cb50b92b0dd))

## [1.1.0](https://gitlab.mim-libre.fr/EOLE/eole-3/services/sondage/compare/release/1.0.0...release/1.1.0) (2023-02-01)


### Features

* publish sondage 1.6.0 ([0b8d6fc](https://gitlab.mim-libre.fr/EOLE/eole-3/services/sondage/commit/0b8d6fcb894a78c932655b51174242b7b47215b8))

## 1.0.0 (2022-12-09)


### Features

* **ci:** build helm package ([52c063f](https://gitlab.mim-libre.fr/EOLE/eole-3/services/sondage/commit/52c063f15574daa7a6c692603bdb2f5173aea0b5))
* **ci:** push helm package to chart repository ([e13ce0c](https://gitlab.mim-libre.fr/EOLE/eole-3/services/sondage/commit/e13ce0c9728276310399e6920847c4bf85d92d6b))
* **ci:** update helm chart version with `semantic-release` ([218158d](https://gitlab.mim-libre.fr/EOLE/eole-3/services/sondage/commit/218158dd1d100c787673aa76b4866c8f2dce0d8b))
* **ci:** validate helm chart at `lint` stage ([c27e3ad](https://gitlab.mim-libre.fr/EOLE/eole-3/services/sondage/commit/c27e3ad874d6212a27621c15472608030e01abf4))
* update app version to 1.5.0 ([0f109a8](https://gitlab.mim-libre.fr/EOLE/eole-3/services/sondage/commit/0f109a8752b2df4b7a6babaabc1044e34bbd5104))
* use chart.yaml as default version ([461f750](https://gitlab.mim-libre.fr/EOLE/eole-3/services/sondage/commit/461f750764c504f0629073dcff864faeccdbc865))


### Bug Fixes

* **sondage:** add containerport default value ([b0a271b](https://gitlab.mim-libre.fr/EOLE/eole-3/services/sondage/commit/b0a271b7b08145fbf1f1f5f6855f8368eadc11e4))
* **sondage:** add image repository default value ([8f7627e](https://gitlab.mim-libre.fr/EOLE/eole-3/services/sondage/commit/8f7627e4bbe326f29025c938e774c95b18a2bc16))
* update hpa api version removed on k8s 1.26 ([4ed50a9](https://gitlab.mim-libre.fr/EOLE/eole-3/services/sondage/commit/4ed50a9eacbe61e152f7072269e76c37785f9013))


### Continuous Integration

* **commitlint:** enforce commit message format ([5431e6e](https://gitlab.mim-libre.fr/EOLE/eole-3/services/sondage/commit/5431e6e6ab09b744fbc505fef140ce3d7e7f18b5))
* **release:** create release automatically with `semantic-release` ([64faf24](https://gitlab.mim-libre.fr/EOLE/eole-3/services/sondage/commit/64faf244e710da9eca17649f24bcbf3f9e3e3810))
